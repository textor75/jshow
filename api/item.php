<?
$title = 'API';
include('_header.php');
?>
<h2>Постоянные параметры элементов.</h2>

<div class="jumbotron">
    <p>
        Все нижеописанные параметры (постоянные и изменяемые) вызываются от объекта элемента, получаемого методом
        <a href="jshow.php#item"><code>jShow.item(item_name)</code></a>.
    </p>
    <p>
        При показе презентации постоянные параметры не изменяются, принимая последнее объявленное значение.
        При вызове без аргументов, постоянные параметры возвращают текущее значение, иначе сам объект элемента.
    </p>
</div>

<h3 id="align"><code>align()</code></h3>
<pre><code class="javascript">jShow.item('test').align(mode);</code></pre>

<p>Определяет горизонтальное выравнивание содержимого внутри элемента</p>

<p>
    <code>mode</code> - (строка) значение, соответствующее css свойству text-align:<br>
    - <code>'left'</code> - выравнивание по левому краю (по умолчанию);<br>
    - <code>'center'</code> - выравнивание по центру;<br>
    - <code>'right'</code> - выравнивание по правому краю;<br>
    - <code>'justify'</code> - выравнивание по ширине.
</p>

<hr>

<h3 id="back"><code>back()</code></h3>
<pre><code class="javascript">jShow.item('test').back(path);</code></pre>

<p>
    Определяет фоновое изображение для элемента.
    Изображение растягивается таким образом, чтобы целиком покрыть элемент.
    При этом равномерно обрезаются верхний и нижний или левый и правый края изображения.
</p>

<p>
    <code>path</code> - (строка) полный или относительный (относительно html-файла презентации) путь до изображения,
    например: <code>'../img/back.jpg'</code>.
</p>

<hr>

<h3 id="center"><code>center()</code></h3>
<pre><code class="javascript">jShow.item('test').center(percentX, percentY);</code></pre>

<p>
    Определяет центр (в процентах), вокруг которого происходит трансформация элемента, например, вращение.
</p>

<p>
    <code>percentX</code> - (число) отступ от левого края<br>
    <code>percentY</code> - (число) отступ от верхнего края<br>
    например: <code>20, 30</code>.
</p>

<p>Значение по умолчанию: 50% 50%.</p>

    <hr>

<h3 id="html"><code>html()</code></h3>
<pre><code class="javascript">jShow.item('test').html(code);</code></pre>

<p>Устанавливает html-содержимое элемента (переопределяет ранее заданный контент).</p>

<p>
    <code>code</code> - (строка) html-код,
    например: <code>'&lt;strong&gt;jShow&lt;/strong&gt; - это...'</code>.
</p>

    <hr>

<h3 id="img"><code>img()</code></h3>
<pre><code class="javascript">jShow.item('test').img(path);</code></pre>

<p>
    Располагает внутри элемента изображение (затирая существующий контент).
    По умолчанию картинка растягиватеся на всю ширину элемента.
    Если для элемента не задана высота, он подстроится под высоту изображения.
    Если высота элемента определена и она не позволяет изображению поместиться целиком,
    низ картинки будет обрезан.
</p>

<p>
    <code>path</code> - (строка) полный или относительный (относительно html-файла презентации) путь до изображения,
    например: <code>'../img/image.png'</code>.
</p>

    <hr>

<h3 id="link"><code>link()</code></h3>
<pre><code class="javascript">jShow.item('test').link(href);</code></pre>

<p>
    Делает элемент ссылкой. Ссылка всегда открывается в новом окне (закладке).
</p>

<p>
    <code>href</code> - (строка) ссылка в любом виде,
    например: <code>'http://jshow.meum.ru/'</code>.
</p>

    <hr>

<h3 id="relate"><code>relate()</code></h3>
<pre><code class="javascript">jShow.item('test').relate(parent_item);</code></pre>

<p>
    Вкладывает элемент внутрь другого.
    Это позволяет перемещать или вращать родительский элемент вместе с дочерними.
    Допустимо также множественное вложение:
    как вкладывание внутрь вложенного в другой элемент, так вложение нескольких элементов в один.<br>
    <a href="../examples/relate">См. пример</a>
</p>

<p>
    <code>parent_item</code> - (строка) имя родительского элемента.
</p>

    <hr>

<h3 id="text"><code>text()</code></h3>
<pre><code class="javascript">jShow.item('test').text(text [, ...]);</code></pre>

<p>
    Устанавливает текст для элемента (очищая содержимое элемента).
    Текст отображается в том виде, в котором задан, без обработки html-сущностей.
</p>

<p>Если передать несколько аргументов, например, <code>text('Hello', '', 'World!')</code>, то текст будет многострочным.</p>

<p>
    <code>text</code> - (строка) текст для вывода в элемент.
</p>

    <hr>

<h3 id="type"><code>type()</code></h3>
<pre><code class="javascript">jShow.item('test').type(type_name);</code></pre>

<p>
    Устанавливает тип элемента. Css-правила для типа могут быть заданы как в файле jshow.css,
    так и добавлены или переопределены в пользовательской таблице стилей.
</p>

<p>
    <code>type_name</code> - (строка) имя типа. На данный момент существуют следующие предопределенные типы:<br>
    - <code>'baloon'</code> - крупный округлый элемент с рамкой;<br>
    - <code>'console'</code> - темная плашка с моноширинным шрифтом.
</p>

    <hr>

<h3 id="video"><code>video()</code></h3>
<pre><code class="javascript">jShow.item('test').video(path [, type]);</code></pre>

<p>
    Располагает внутри элемента видеоролик (затирая существующий контент).
</p>

<p>
    <code>path</code> - (строка) полный или относительный (относительно html-файла презентации) путь к видеофайлу.<br>
    <code>type</code> - (строка) тип ролика, необязательный параметр, например: <code>'ogg'</code>. Значение по умолчанию: mp4.
</p>

<h2>Изменяемые параметры элементов.</h2>

<div class="jumbotron">
    <p>
        Нижеописанные параметры могут быть изменены от страницы к странице (будут плавно изменяться при смене слайдов),
        но не изменяются внутри одного слайда.
    </p>
    <p>Данные методы всегда возвращают сам объект элемента.</p>
</div>

<h3 id="backcolor"><code>backColor()</code></h3>
<pre><code class="javascript">jShow.item('test').backColor(color1 [, color2, color3 [, alpha] ]);</code></pre>

<p>Устанавливает цвет фона элемента.</p>

<p>
    Может принимать 1, 3 или 4 параметра:<br>
    <code>color1</code> - (строка) при единственном аргументе - цвет в шестнадцатеричном или допустимом для css текстовом формате;<br>
    <code>color1 color2 color3</code> - (числа) при трех аргументах - значение RGB-каналов от 0 до 255;<br>
    <code>alpha</code> - (число) в качестве четвертого аргумента - альфа-канал от 0 до 1.
</p>

    <hr>

<h3 id="bottom"><code>bottom()</code></h3>
<pre><code class="javascript">jShow.item('test').bottom(distance);</code></pre>

<p>
    Устанавливает отступ элемента от нижнего края презентации.
    Параметр <code>top()</code> при этом сбрасывается.
</p>

<p><code>distance</code> - (число) расстояние согласно установленного разрешения.</p>

    <hr>

<h3 id="color"><code>color()</code></h3>
<pre><code class="javascript">jShow.item('test').color(color);</code></pre>

<p>Определяет цвет текста для элемента.</p>

<p>
    <code>color</code> - (строка) цвет в шестнадцатеричном или допустимом для css текстовом формате,
    например: <code>'#2103CC'</code> или <code>'blue'</code>.
</p>

    <hr>

<h3 id="fontsize"><code>fontSize()</code></h3>
<pre><code class="javascript">jShow.item('test').fontSize(size);</code></pre>

<p>Определяет размер шрифта внутри элемента.</p>

<p>
    <code>size</code> - (число) размер относительно базового шрифта, равному 1.
    Например: <code>0.8</code> или <code>2</code>.
</p>

    <hr>

<h3 id="height"><code>height()</code></h3>
<pre><code class="javascript">jShow.item('test').height(size);</code></pre>

<p>Определяет высоту элемента.</p>

<p><code>size</code> - (число) размер согласно установленного разрешения.</p>

    <hr>

<h3 id="hide"><code>hide()</code></h3>
<pre><code class="javascript">jShow.item('test').hide();</code></pre>

<p>Скрывает элемент. На самом деле устанавливает <code>opacity: 0</code> и <code>z-index: -1</code>.</p>

    <hr>

<h3 id="left"><code>left()</code></h3>
<pre><code class="javascript">jShow.item('test').left(distance);</code></pre>

<p>
    Устанавливает отступ элемента от левого края презентации.
    Параметр <code>right()</code> при этом сбрасывается.
</p>

<p><code>distance</code> - (число) расстояние согласно установленного разрешения.</p>

    <hr>

<h3 id="opacity"><code>opacity()</code></h3>
<pre><code class="javascript">jShow.item('test').opacity(value);</code></pre>

<p>Определяет непрозрачность элемента.</p>

<p><code>value</code> - (число) непрозрачность от <code>0</code> (полностью прозрачый) до <code>1</code> (полностью непрозрачый).</p>

    <hr>

<h3 id="order"><code>order()</code></h3>
<pre><code class="javascript">jShow.item('test').order(index);</code></pre>

<p>Меняет <code>z-index</code> - расположение элемента относительно других.</p>

<p><code>index</code> - (число) элемент с бóльшим индексом находится над элементом с меньшим индексом.</p>

    <hr>

<h3 id="right"><code>right()</code></h3>
<pre><code class="javascript">jShow.item('test').right(distance);</code></pre>

<p>
    Устанавливает отступ элемента от правого края презентации.
    Параметр <code>left()</code> при этом сбрасывается.
</p>

<p><code>distance</code> - (число) расстояние согласно установленного разрешения.</p>

    <hr>

<h3 id="rotate"><code>rotate()</code></h3>
<pre><code class="javascript">jShow.item('test').rotate(deg);</code></pre>

<p>
    Поворачивает элемент по или против часовой стрелки.
    Поворот осуществляется вокруг центра элемента (см. <a href="#center"><code>center()</code></a>).
</p>

<p>
    <code>deg</code> - (число) поворот в градусах, положительное значение по часовой стрелке.
    Например: <code>-45</code> - поворот на 45° против часовой стрелки.
</p>

    <hr>

<h3 id="rotatex"><code>rotateX()</code></h3>
<pre><code class="javascript">jShow.item('test').rotateX(deg);</code></pre>

<p>
    Поворачивает элемент вокруг горизонтальной оси.
    Ось проходит через центр элемента (см. <a href="#center"><code>center()</code></a>).
</p>

<p><code>deg</code> - (число) поворот в градусах, положительное значение по часовой стрелке.</p>

    <hr>

<h3 id="rotatey"><code>rotateY()</code></h3>
<pre><code class="javascript">jShow.item('test').rotateY(deg);</code></pre>

<p>
    Поворачивает элемент вокруг вертикальной оси.
    Ось проходит через центр элемента (см. <a href="#center"><code>center()</code></a>).
</p>

<p><code>deg</code> - (число) поворот в градусах, положительное значение по часовой стрелке.</p>

    <hr>

<h3 id="round"><code>round()</code></h3>
<pre><code class="javascript">jShow.item('test').round(percent1 [, percent2 [, percent3 [, percent4]]]);</code></pre>

<p>Определяет "округлость" элемента в процентах.</p>

<p>
    <code>percent1</code> - (число) <code>0</code> - прямые углы у элемента, <code>100</code> - полностью скругленные углы.
    Наличие других аргументов определяет округлость разных углов в том же порядке,
    как это установлено для css-свойства border-radius.
</p>

    <hr>

<h3 id="show"><code>show()</code></h3>
<pre><code class="javascript">jShow.item('test').show();</code></pre>

<p>Отображает элемент (по умолчанию скрыт). На самом деле устанавливает <code>opacity: 1</code> и <code>z-index: 1</code>.</p>

    <hr>

<h3 id="top"><code>top()</code></h3>
<pre><code class="javascript">jShow.item('test').top(distance);</code></pre>

<p>
    Устанавливает отступ элемента от верхнего края презентации.
    Параметр <code>bottom()</code> при этом сбрасывается.
</p>

<p><code>distance</code> - (число) расстояние согласно установленного разрешения.</p>

    <hr>

<h3 id="width"><code>width()</code></h3>
<pre><code class="javascript">jShow.item('test').width(size);</code></pre>

<p>Определяет ширину элемента.</p>

<p><code>size</code> - (число) размер согласно установленного разрешения.</p>

    <hr>

<h3 id="zoom"><code>zoom()</code></h3>
<pre><code class="javascript">jShow.item('test').zoom(size);</code></pre>

<p>
    Увеличивает или уменьшает объект.
    Трансформация происходит относительно базовой точки (см. <a href="#center"><code>center()</code></a>).
    Значение свойств <code>width</code>, <code>left</code> и пр. не изменяются
    и применяются относительно габаритов элемента без увеличения (<code>zoom(1)</code>).
</p>

<p><code>size</code> - (число) коэффициент изменения размера. Значение больше 1 увеличивает элемент, меньше - уменьшает.</p>

<? include('_footer.php');