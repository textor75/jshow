<?
$title = 'Кастомизация стилей';
include('_header.php');
?>

<p>
    При необходимости можно подключить к презентации собственный файл стилей.
    В нем вы можете кастомизировать вид:
</p>
<ul>
    <li>служебных элемнотов презентации;</li>
    <li>всех элементов презентации;</li>
    <li>отдельных типов элементов презентации;</li>
    <li>каждого отдельного элемента.</li>
</ul>

<h4>Служебные элементы</h4>

<h6>Кнопка &laquo;Показать в отдельном окне&raquo;</h6>

<p>
    Данная кнопка отображается в правом нижнем углу только в том случае, если презентация отображается во фрейме.
    Для ее изменения достаточно переопределить стили для <code>#jshow-popout</code>, например:
</p>

<pre><code class="css">#jshow-popout {
    background-color: rgba(120, 120, 120, .7);
}</code></pre>

<h6>Полоса таймера</h6>

<p>
    Данная полоса отображается при вызове в сценарии метода <a href="../api/jshow.php#time"><code>jShow.time()</code></a>.<br>
    Есть возможность кастомизировать:
</p>

<ul>
    <li><code>#jshow-timer-container</code> - сама полоса таймера;</li>
    <li><code>#jshow-timer</code> - полоса прошедшего времени;</li>
    <li><code>#jshow-pager</code> - маркер положения текущей страницы.</li>
</ul>

<h6>Субтитры</h6>

<p>Для переопределения внешнего вида субтитров, нужно править стили для <code>#jshow-subtitles</code>, например:</p>

<pre><code class="css">#jshow-subtitles {
    font-size: 16pt;
}</code></pre>

<h4>Все элементы презентации</h4>

<p>Общий стиль для всех элементов презентации задается классом <code>.jshow__item</code>, например:</p>

<pre><code class="css">.jshow__item {
    transition-duration: 2s;
}</code></pre>

<h4>Отдельные типы элементов</h4>

<p>Если в презентации мы определяем тип какого-либо элемента, например: </p>

<pre><code class="javascript">jShow.item('test').type('baloon');</code></pre>

<p>то мы можем определить (или переопределить) его стиль через селектор аттрибута:</p>

<pre><code class="css">.jshow__item[data-type="baloon"] {
    border-radius: 50%;
}</code></pre>

<h4>Каждый элемент</h4>

<p>Каждый элемент задается через уникальное имя, например:</p>

<pre><code class="javascript">jShow.item('hello');</code></pre>

<p>Такой элемент мы можем кастомизировать через стиль:</p>

<pre><code class="css">#jshow__item-hello {
    align: right;
}</code></pre>

<p>Данный пример выполняет выравнивание текста в конкретном элементе. Также это можно сделать и через сценарий.</p>

<pre><code class="javascript">jShow.item('hello').align('right');</code></pre>

<? include('_footer.php');