<?
$file = preg_replace('/[^a-z.]+/im', '', $_GET['file'] ?: '');
if (!$file || !is_file(__DIR__ . '/' . $file . '.js')) {
    die;
}
$t = time();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE"/>
    <link rel="stylesheet" type="text/css" href="../src/jshow/jshow.css?<?= $t ?>">
    <link rel="stylesheet" type="text/css" href="../css/docs.css?<?= $t ?>">
    <script src="../src/jshow/jshow.js?<?= $t ?>"></script>
</head>
<body>
<script src="<?= $file ?>.js?<?= $t ?>"></script>
</body>
</html>
