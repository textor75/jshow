jShow.resolution(1920, 1080).backColor('#c8bab8');

    jShow.item('title').text('Презентация в браузере').left(100).top(80).fontSize(1.2);
    jShow.item('jshow').text('jShow').left(100).top(140).fontSize(3);
    jShow.item('more').link('http://jshow.meum.ru').text('Подробнее').left(100).bottom(80);

    jShow.item('control').text(
        'Для навигации можно использовать:',
        '',
        '▶ Следующий слайд', 'движение влево на тачскрине, клавиши ↑, → или пробел;',
        '',
        '◀ Предыдущий слайд', 'движение вправо на тачскрине, клавиши ↓ или  ←.'
    ).left(-3000).top(380).show();


jShow.page('index');

    jShow.item('title').show();
    jShow.item('jshow').show();
    jShow.item('control').left(100);
    jShow.item('more').show();
    jShow.item('travolta-img').img('../img/travolta.png')
        .width(600).bottom(0).right(100)
        .show();

    jShow.item('presentation').text(
        'С помощью jShow вы можете сделать более привлекательным урок или доклад.',
        '', 'Также вы можете оформить на своем сайте страницу с инструкциями, рецептами, схемами и т.д.'
    ).width(1720).top(380).left(3200).show();

    jShow.item('star-img').img('../img/star.png')
        .width(0).top(600).left(960);


jShow.page('presentation');

    jShow.item('control').left(-3000).hide();
    jShow.item('travolta-img').width(0).hide();
    jShow.item('presentation').left(100);
    jShow.item('star-img').width(600).left(660).show();
    jShow.item('need').text('Что для этого нужно?').type('baloon').fontSize(1.6).width(600).left(660).top(-600).show();

jShow.page('need');

    jShow.item('presentation').left(-3000).hide();
    jShow.item('star-img').width(0).left(960).top(1080).hide();
    jShow.item('need').top(400);

    jShow.item('notepad1').text('Для создания презентации:').left(280).top(600);
    jShow.item('notepad2').text('Любой текстовой редактор').left(-1000).top(740).show();
    jShow.item('notepad-img').img('../img/notepad.png')
        .width(0).top(625).left(175);

jShow.page('notepad');

    jShow.item('need').top(200);
    jShow.item('notepad1').show();
    jShow.item('notepad2').left(100);
    jShow.item('notepad-img').width(150).left(100).top(550).show();

    jShow.item('browser1').text('Для просмотра презентации:').right(280).top(600);
    jShow.item('browser2').text('Любой современный браузер').right(-1000).top(740).show();
    jShow.item('browser-img').img('../img/chrome.png')
        .width(0).top(625).right(175);

jShow.page('browser');

    jShow.item('browser1').show();
    jShow.item('browser2').right(100);
    jShow.item('browser-img').width(150).right(100).top(550).show();

    jShow.item('fancy').text('И немного фантазии').bottom(-200).left(460).width(1000).align('center').fontSize(1.5).show();

jShow.page('fancy');

    jShow.item('fancy').bottom(150);

    jShow.item('next').type('baloon').html('<a href="./start.php">Далее</a>').fontSize(1.6).width(600).left(660).top(400);

    /** @TODO Запускается локально и без веб-серверов, Подстраивается под любой экран */

jShow.page('next');

    jShow.item('title').left(-600).top(-600);
    jShow.item('jshow').text('jShow').left(-600).top(-400);

    jShow.item('need').top(-600).hide();
    jShow.item('fancy').bottom(-400);

    jShow.item('notepad1').left(-1000);
    jShow.item('notepad2').left(-1000);
    jShow.item('notepad-img').left(-1200);

    jShow.item('browser1').right(-1000);
    jShow.item('browser2').right(-1000);
    jShow.item('browser-img').right(-1200);

    jShow.item('next').show();


