<?
$title = 'Быстрый старт';
include('_header.php');
?>

<p>
    Первым делом нужно <a href="https://bitbucket.org/textor75/jshow/src/master/src/jshow/" target="_blank">скачать файлы</a>
    jshow.js и jshow.css и подключить их в своем документе внутри <code>&lt;head&gt;&lt;/head&gt;</code>.
</p>

<pre><code class="html">&lt;link rel="stylesheet" type="text/css" href="jshow/jshow.css"&gt;
&lt;script src="jshow/jshow.js"&gt;&lt;/script&gt;</code></pre>

<p>
    При необходимости можно подключить <a href="css.php">свой файл стилей</a>.
</p>

<p>
    После этого в теле html-страницы подключаем файл со сценарием презентации
</p>

<pre><code class="html">&lt;body&gt;
    &lt;script src="hello.js"&gt;&lt;/script&gt;
&lt;/body&gt;</code></pre>

<p>
    Важно иметь ввиду, что все элементы в теле страницы будут удалены.
</p>

<p>
    В файле сценария (в данном примере <code>hello.js</code>) сначала определяем стили презентации.
</p>

<pre><code class="javascript">// Определяем разрешение (относительное) и фон презентации.
jShow.resolution(1000, 750).backColor('#667178');</code></pre>

<p>Потом объявляем первый слайд с уникальным именем.</p>

<pre><code class="javascript">jShow.page('slide1');</code></pre>

<p>Располагаем элемент на слайде.</p>

<pre><code class="javascript">jShow.item('hello').text('Hello World!')
    .top(300).left(100).width(800).type('baloon').show();</code></pre>

<p>На следующей странице делаем переход.</p>

<pre><code class="javascript">jShow.page('slide2');

jShow.item('hello').top(1000);
jShow.item('bye').text('Bye!')
    .top(350).left(100).width(800).align('center').fontSize(1.5).show();</code></pre>

<p>Весь код и результат можно посмотреть в <a href="../examples/hello">разделе с примерами</a>.</p>

<? include('_footer.php');