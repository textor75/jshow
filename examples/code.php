<?
$file = preg_replace('/[^a-z.]+/im', '', $_GET['file'] ?: '');
if (!$file || !is_file(__DIR__ . '/scenario/' . $file . '.js')) {
	die;
}
?>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.11.0/styles/hopscotch.min.css">
		<style>
			* {
				padding: 0;
				margin: 0;
				box-sizing: border-box;
			}
			html, body {
				position: relative;
				height: 100%;
                background: #322931;
			}
			pre {
				display: block;
				width: 100%;
				min-height: 100%;
                position: relative;
			}

            pre > code {
                height: 100%;
            }
		</style>
		<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.11.0/highlight.min.js"></script>
		<script>hljs.initHighlightingOnLoad();</script>
	</head>
	<body>
		<pre><code class="javascript"><?= htmlspecialchars(file_get_contents(__DIR__ . '/scenario/' . $file . '.js')); ?></code></pre>
	</body>
</html>