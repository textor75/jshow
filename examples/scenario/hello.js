// Определяем разрешение (относительное) и фон презентации.
jShow.resolution(1000, 750).backColor('#667178');

jShow.page('slide1');

jShow.item('hello').text('Hello World!')
    .top(300).left(100).width(800).type('baloon').show();

jShow.page('slide2');

jShow.item('hello').top(1000);
jShow.item('bye').text('Bye!')
    .top(350).left(100).width(800).align('center').fontSize(1.5).show();
