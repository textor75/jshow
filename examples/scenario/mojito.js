jShow.debug(0).resolution(1920, 1080).backColor('#142d43').back('../../img/lights.jpg').color('#FFF');

        jShow.item('recipe').text('рецепты коктейлей').left(150).width(500).top(120).color('#b5d190');
        jShow.item('mojito').text('Мохито').left(150).width(500).top(180).type('mojito').fontSize(3.6);
        jShow.item('jshow').link('http://jshow.meum.ru').text('Сделано на jShow').right(50).bottom(30).fontSize(.5);
        jShow.item('control').text(
            'Для просмотра страниц используйте',
            'стрелки на клавиатуре или',
            'горизонтальный жест на тачскрине',
        ).left(400).width(1120).top(500).color('#00123c').align('center');
        jShow.item('more').link('http://jshow.meum.ru').text('Подробнее').left(400).width(1120).top(700).color('#00123c').align('center');

jShow.page('mojito');

        jShow.item('mojito').show();
        jShow.item('recipe').show();
        jShow.item('jshow').show().opacity(.4);
        jShow.item('control').show();
        jShow.item('more').show();

        jShow.item('glass').img('../../img/glass.png').width(200).left(4000).bottom(100).order(50).show();
        jShow.item('ingredients').text('Ингредиенты').right(200).width(500).top(-100).fontSize(2);

jShow.page('glass').sound('../../media/slip.mp3');

        jShow.item('control').hide();
        jShow.item('more').hide();
        jShow.item('glass').left(200);
        jShow.item('ingredients').top(120).show();

        jShow.item('lime').text('лайм').right(240).top(1200).show();
        jShow.item('lime-m').text('½ шт').left(3000).top(240).show();
        jShow.item('lime-img').img('../../img/lime.png').width(80).bottom(400).left(-200).rotate(0).show();

jShow.page('lime').sound('../../media/slip.mp3');

        jShow.item('lime').top(240);
        jShow.item('lime-m').left(1720);
        jShow.item('lime-img').left(260);

        jShow.item('mint').text('листья мяты').right(240).top(1200).show();
        jShow.item('mint-m').text('10 шт').left(3000).top(300).show();
        jShow.item('mint-img').img('../../img/mint.png').width(50).bottom(-100).left(800).rotate(-45).show();

jShow.page('mint').sound('../../media/slip.mp3');

        jShow.item('mint').top(300);
        jShow.item('mint-m').left(1720);
        jShow.item('mint-img').rotate(45).bottom(400).left(400);

        jShow.item('sugar').text('коричневый сахар').right(240).top(1200).show();
        jShow.item('sugar-m').text('2 ч/лож.').left(3000).top(360).show();
        jShow.item('sugar-img').img('../../img/sugar.png').width(100).bottom(400).left(500).height(0).show();

jShow.page('sugar').sound('../../media/slip.mp3');

        jShow.item('sugar').top(360);
        jShow.item('sugar-m').left(1720);
        jShow.item('sugar-img').height(60);

        jShow.item('ice').text('дроблёный лед').right(240).top(1200).show();
        jShow.item('ice-m').text('200 г').left(3000).top(420).show();
        jShow.item('ice-img').back('../../img/ice.jpg').width(0).height(0).bottom(400).left(700).round(100, 100, 0, 0).show().opacity(.5);

jShow.page('ice').sound('../../media/slip.mp3');

        jShow.item('ice').top(420);
        jShow.item('ice-m').left(1720);
        jShow.item('ice-img').width(100).height(100).left(650);

        jShow.item('rum').text('белый ром').right(240).top(1200).show();
        jShow.item('rum-m').text('50 мл').left(3000).top(480).show();
        jShow.item('rum-img').img('../../img/rum.png').width(160).bottom(1200).left(820).center(0, 100).rotate(0).show();

jShow.page('rum').sound('../../media/slip.mp3');

        jShow.item('rum').top(480);
        jShow.item('rum-m').left(1720);
        jShow.item('rum-img').bottom(390);

        jShow.item('soda').text('содовая').right(240).top(1200).show();
        jShow.item('soda-m').text('100 мл').left(3000).top(540).show();
        jShow.item('soda-img').img('../../img/soda.png').width(160).bottom(-500).left(1040).center(0, 100).rotate(0).show();

jShow.page('soda').sound('../../media/slip.mp3');

        jShow.item('soda').top(540);
        jShow.item('soda-m').left(1720);
        jShow.item('soda-img').bottom(390);

        jShow.item('step1').text('Разрежьте лайм на 4 дольки.').top(780).left(500).width(1100);
        jShow.item('slice1-img').img('../../img/slice.png').width(80).bottom(400).left(260).rotate(90);
        jShow.item('slice2-img').img('../../img/slice.png').width(80).bottom(400).left(260).rotate(-90);

jShow.page('step1');

        jShow.item('step1').show();
        jShow.item('slice1-img').rotate(270).show();
        jShow.item('slice2-img').rotate(135).bottom(130).show();
        jShow.item('lime-img').rotate(180).hide();

        jShow.item('step2').text(
            'Положите листья мяты и 2 дольки лайма в стакан из толстого стекла.',
            'Подавите мяту и лайм мадлером или ложкой, чтобы они пустили сок.'
        ).top(780).left(500).width(1100);
        jShow.item('mint2-img').img('../../img/mint.png').width(50).bottom(400).left(400).rotate(45).show();

jShow.page('step2');

        jShow.item('step1').hide();
        jShow.item('step2').show();
        jShow.item('slice1-img').bottom(300).left(120).rotate(115);
        jShow.item('mint-img').bottom(300).left(400).rotate(-45);
        jShow.item('mint2-img').bottom(180).left(270).rotate(90).show();
        jShow.item('sugar-img').left(250);
        jShow.item('ice-img').left(400);

        jShow.item('step3').text('Добавьте ещё 1 дольку лайма и сахар, а после снова подавите.').top(780).left(500).width(1100);

jShow.page('step3');

        jShow.item('step2').hide();
        jShow.item('step3').show();
        jShow.item('sugar-img').bottom(130).hide();
        jShow.item('ice-img').left(250);

        jShow.item('step4').text('Заполните стакан льдом почти до самого верха.').top(780).left(500).width(1100);

jShow.page('step4').sound('../../media/ice.mp3');

        jShow.item('step3').hide();
        jShow.item('step4').show();
        jShow.item('ice-img').left(225).bottom(130).width(150).height(100).round(0, 0, 20, 20);

        jShow.item('step5').text('Налейте ром на лёд.').top(780).left(500).width(1100);
        jShow.item('rum-shot').backColor(84, 80, 50, .4).left(225).bottom(130).width(150).height(0).round(0).show();

jShow.page('step5').sound('../../media/rum.mp3');

        jShow.item('step4').hide();
        jShow.item('step5').show();
        jShow.item('rum-img').rotate(-90).left(920);
        jShow.item('rum-shot').height(90).round(0, 0, 20, 20);

        jShow.item('step6').text('Добавьте содовую.').top(780).left(500).width(1100);

jShow.page('step6').sound('../../media/soda.mp3');

        jShow.item('step5').hide();
        jShow.item('step6').show();
        jShow.item('rum-img').left(-3000);
        jShow.item('rum-shot').backColor(90, 98, 84, .4).height(140);
        jShow.item('soda-img').rotate(-90).left(700);

        jShow.item('step7').text(
            'Аккуратно размешайте коктейльной ложкой и добавьте больше сахара, если это необходимо.',
            '', 'Украсьте готовый коктейль долькой лайма и листиками мяты.'
        ).top(450).left(150).width(1150);

jShow.page('step7');

        jShow.item('step6').hide();
        jShow.item('step7').show();
        jShow.item('soda-img').left(-300);
        jShow.item('slice1-img').bottom(260).left(180);
        jShow.item('mint-img').bottom(260).left(350);

jShow.page('end');

        jShow.item('step7').hide();
        jShow.item('glass').width(400).left(760);
        jShow.item('slice2-img').width(160).bottom(160).left(880);
        jShow.item('mint2-img').width(100).bottom(260).left(900);
        jShow.item('ice-img').left(810).bottom(160).width(300).height(200);
        jShow.item('rum-shot').left(810).bottom(160).width(300).height(280);
        jShow.item('slice1-img').width(160).bottom(420).left(720);
        jShow.item('mint-img').width(100).bottom(420).left(1060);