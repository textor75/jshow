jShow.debug(0).resolution(1920, 1080).backColor('#c8bab8');

    jShow.page('act1');

jShow.item('floor').backColor('#474103')
    .width(1920).height(440).left(0).top(640)
    .show();

jShow.item('wheel').type('lever').backColor('#82a1d7')
    .width(200).height(200).round(100).left(50).top(440).zoom(1).rotate(0)
    .show();

jShow.item('arm').type('lever').backColor('#FFF').width(400).height(50).left(75).top(0).rotate(-90).center(6.25, 50)
    .relate('wheel').show();
jShow.item('hand').type('lever').backColor('#CCC').width(400).height(50).left(150).top(75)
    .relate('wheel').show();

    jShow.page('act2');

jShow.item('wheel').left(365).rotate(180);
jShow.item('arm').rotate(-270);


    jShow.page('act3');

jShow.item('wheel').left(1310).rotate(720);
jShow.item('arm').rotate(-810);

    jShow.page('act4');

jShow.item('wheel').left(860).top(440).zoom(2).rotate(720);
