<!DOCTYPE html>
<html class="menu">
	<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE"/>

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
		<link rel="stylesheet" href="css/style.css?<?= time() ?>">
		
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>		
		
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

	</head>
	<body>
<?
function getId($link) {
    $link = str_replace('.php', '', $link);
    $link = preg_replace('/[^a-z0-9]+/im', '-', $link);
    return trim($link, '-');
}

$menu = [
	[
		'title' => 'Документация',
		'items' => [
			[
				'items' => [
					'docs/init' => 'Введение',
					'docs/start.php' => 'Быстрый старт',
                    'docs/css.php' => 'Кастомизация стилей'
				],
			],
		],
	],
	[
		'title' => 'API',
		'items' => [
			[
                'title' => 'Методы презентации',
				'items' => [
                    'api/jshow.php#back' => 'back()',
                    'api/jshow.php#backcolor' => 'backColor()',
                    'api/jshow.php#color' => 'color()',
                    'api/jshow.php#debug' => 'debug()',
                    'api/jshow.php#resolution' => 'resolution()',
                    'api/jshow.php#time' => 'time()',
				],
			],
            [
                'items' => [
                    'api/jshow.php#item' => 'item()',
                    'api/jshow.php#page' => 'page()',
                ],
            ],
			[
				'title' => 'Постоянные параметры элементов',
				'items' => [
                    'api/item.php#align' => 'align()',
                    'api/item.php#back' => 'back()',
                    'api/item.php#center' => 'center()',
                    'api/item.php#html' => 'html()',
                    'api/item.php#img' => 'img()',
                    'api/item.php#link' => 'link()',
                    'api/item.php#relate' => 'relate()',
                    'api/item.php#text' => 'text()',
                    'api/item.php#type' => 'type()',
                    'api/item.php#video' => 'video()',
				],
			],
            [
                'title' => 'Изменяемые параметры',
                'items' => [
                    'api/item.php#backcolor' => 'backColor()',
                    'api/item.php#bottom' => 'bottom()',
                    'api/item.php#color' => 'color()',
                    'api/item.php#fontsize' => 'fontSize()',
                    'api/item.php#height' => 'height()',
                    'api/item.php#hide' => 'hide()',
                    'api/item.php#left' => 'left()',
                    'api/item.php#opacity' => 'opacity()',
                    'api/item.php#order' => 'order()',
                    'api/item.php#right' => 'right()',
                    'api/item.php#rotate' => 'rotate()',
                    'api/item.php#rotatex' => 'rotateX()',
                    'api/item.php#rotatey' => 'rotateY()',
                    'api/item.php#round' => 'round()',
                    'api/item.php#show' => 'show()',
                    'api/item.php#top' => 'top()',
                    'api/item.php#width' => 'width()',
                    'api/item.php#zoom' => 'zoom()',
                ],
            ],
            [
                'title' => 'Методы страницы',
                'items' => [
                    'api/page.php#sound' => 'sound()',
                    'api/page.php#text'  => 'text()',
                    'api/page.php#time'  => 'time()',
                ],
            ],
		],
	],
	[
		'title' => 'Примеры',
		'items' => [
			[
				'items' => [
                    'examples/hello' => 'Hello World!',
                    'examples/relate' => 'Связка элементов',
					'examples/mojito' => 'Мохито',
				],
			],
		],
	],
//	[
//		'title' => 'Скачать',
//		'items' => [
//			[
//				'items' => [
//					'download/jahow-v.0.0.1.zip' => 'v 0.0.1',
//				],
//			],
//		],
//	],
];
?>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h1>jShow</h1>
					
					<? foreach ($menu as $item1): ?>
						<? if (isset($item1['title'])): ?>
							<h4><?= $item1['title'] ?></h4>
						<? endif ?>
					
						<? foreach ($item1['items'] as $item2): ?>
							<? if (isset($item2['title'])): ?>
								<h6>
									<? if (isset($item2['link'])): ?>
                                        <? $id = getId($item2['link']) ?>
										<a href="/#<?= $id ?>" data-href="<?= $item2['link'] ?>" id="<?= $id ?>" target="_top"><?= $item2['title'] ?></a>
									<? else: ?>
									<?= $item2['title'] ?>
									<? endif ?>
								</h6>
							<? endif ?>
					
							<? if (!empty($item2['items'])): ?>
								<ul>
								<? foreach ($item2['items'] as $link => $title): ?>
                                    <? $id = getId($link) ?>
									<li><a href="/#<?= $id ?>" data-href="<?= $link ?>" id="<?= $id ?>" target="_top"><?= $title ?></a></li>
								<? endforeach ?>
								</ul>
							<? endif ?>
						<? endforeach; ?>
					<? endforeach; ?>
				</div>
			</div>
		</div>
    <script src="script.js?<?= time() ?>"></script>
	</body>
</html>