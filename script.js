jQuery(function($) {
    $('a[data-href]').on('click', function() {
        top.frames.Content.location.href = $(this).data('href');
        return false;
    });

    if (top.location.hash) {
        let link = $('a' + top.location.hash).attr('data-href');
        if (link) {
            top.frames.Content.location.href = link;
        }
    }
});
