/*!
 * jShow
 * http://jshow.meum.ru/
 * Version: 0.0.1
 *
 * Copyright 2019 Alexandr Tkach
 * textor75@gmail.com
 * Released under the MIT license
 * https://bitbucket.org/textor75/jshow/src/master/LICENSE.md
 */
"use strict";
let jShow = new Proxy(new function() {

    let Container = null;
    let Subtitles = null;
    let Start = null;
    let Timer = null;
    let Pagination = null;
    let Pager = null;
    let PagesList = [];
    let PageIndex = 0;

    let Init = function() {
        if (!Container) {
            document.body.innerHtml = '';
            Container = document.createElement('div');
            Container.setAttribute('id', 'jshow-container');
            if (Params.debug[0]) {
                Container.setAttribute('class', 'debug');
            }
            document.body.style.background = '#000';
            document.body.appendChild(Container);
            window.onresize = Resize;
            document.ontouchstart = TouchStart;
            document.ontouchend = TouchEnd;
            for (let name in Params) {
                if (name in ParamHooks) {
                    ParamHooks[name].apply(this, Params[name]);
                } else if (Params[name]) {
                    Container.style[name] = Array.isArray(Params[name]) ? Params[name][0] : Params[name];
                }
            }
            for (let itemId in Items) {
                Items[itemId].create();
            }
            if (window !== window.top) {
                let popout = document.createElement('div');
                popout.setAttribute('id', 'jshow-popout');
                popout.setAttribute('title', 'Popout in a separate window');
                popout.innerHTML = 'popout <span>⭷</span>';
                document.body.appendChild(popout);
                popout.onclick = function() {
                    window.open(location.href, '_blank');
                }
            } else if (document.body.requestFullscreen) {
                document.body.requestFullscreen();
            }

            Subtitles = document.createElement('div');
            Subtitles.setAttribute('id', 'jshow-subtitles');
            Subtitles.setAttribute('data-hidden', 1);
            document.body.appendChild(Subtitles);

            if (Params.time) {
                let timerContainer = document.createElement('div');
                timerContainer.setAttribute('id', 'jshow-timer-container');
                document.body.appendChild(timerContainer);
                Timer = document.createElement('div');
                Timer.setAttribute('id', 'jshow-timer');
                timerContainer.appendChild(Timer);

                Pager = document.createElement('div');
                Pager.setAttribute('id', 'jshow-pager');
                timerContainer.appendChild(Pager);
                Pagination = {};
                let end = 0;
                let start = 0;
                let count = PagesList.length;
                let sumDuration = 0; // Суммарное количество времени показ страниц с определенной длительностью
                let pageAuto = count; // Количество страниц длительность которых определяется автоматически
                let pageId;
                for (let i = 0; i < count; i++) {
                    pageId = PagesList[i];
                    if (Pages.get(pageId).duration !== null) {
                        sumDuration += Pages.get(pageId).duration;
                        pageAuto--;
                    }
                }
                if (!pageAuto || sumDuration > Params.time) {
                    Params.time = sumDuration;
                }
                let duration = pageAuto ? (Params.time - sumDuration) / pageAuto : 0;
                let pageDuration;
                for (let i = 0; i < count; i++) {
                    pageId = PagesList[i];
                    pageDuration = Pages.get(pageId).duration === null ? duration :  Pages.get(pageId).duration;
                    start = end;
                    end += pageDuration * 100 / Params.time;
                    Pagination[PagesList[i]] = [start, end - start];
                }
            }
        }

        setTimeout(function() {
            if (Params.time) {
                Start = (new Date()).getTime();
            }
            let firstPage = PagesList[0];
            if (location.hash && Pages.has(location.hash.substr(1))) {
                firstPage = location.hash.substr(1);
                PageIndex = PagesList.indexOf(firstPage);
                if (PageIndex && Params.time && firstPage in Pagination) {
                    Start -= Params.time[0] * 10 * Pagination[firstPage][0];
                }
            }
            ShowPage(firstPage);
            if (Params.time) {
                updateTimer();
            }
        }, 100);
    };

    let updateTimer = function() {
        let time = (new Date()).getTime();
        let elapsed = time - Start;
        let percent = elapsed / Params.time[0] / 10;
        Timer.style.width = percent + '%';
        let min = Math.floor( elapsed / 60 / 1000 );
        let sec = Math.floor( (elapsed / 1000) % 60 );
        Timer.setAttribute('data-elapsed', min + ':' + (sec < 10 ? '0' : '') + sec);
        if (percent >= 100) {
            percent = 100;
        } else {
            setTimeout(updateTimer, 50);
        }
    };

    let PageSound = new Audio();

    let NextPage = function() {
        if (PageIndex >= PagesList.length - 1) {
            alert('THE END');
        } else {

            PageIndex++;
            let pageId = PagesList[PageIndex];

            if (Pages.has(pageId) && Pages.get(pageId).audio) {
                PageSound.src = Pages.get(pageId).audio;
                PageSound.play();
            }
            ShowPage(pageId);
        }

        return PageIndex;
    };

    let PrevPage = function() {
        if (PageIndex <= 0) {
            alert('THE FIRST PAGE');
        } else {
            Pages.get(CurrentPage).state.forEach(function(itemProperties, itemId) {
                let item = Items[itemId];
                PrepareItem(item, itemProperties.from);
                if (!itemProperties.from.hasOwnProperty('opacity')) {
                    item.div().style.opacity = 0;
                }
                if (!itemProperties.from.hasOwnProperty('order')) {
                    item.div().style.zIndex = -1;
                }
            });
            PageIndex--;

            let newPage = PagesList[PageIndex];
            ShowPage(newPage, true);
        }

        return PageIndex;
    };

    let KeyDown = function(e) {
        let result = false;
        switch(e.keyCode) {
            case 32: // space
            case 38: // up
            case 39: // right
                NextPage();
                break;
            case 40: // down
            case 37: // left
                PrevPage();
                break;
            case 84: // t (е)
                if (Subtitles.hasAttribute('data-hidden')) {
                    Subtitles.removeAttribute('data-hidden');
                } else {
                    Subtitles.setAttribute('data-hidden', 1);
                }
                break;
            default:
                result = true;
        }

        return result;
    };

    let TouchFix = null;

    let TouchStart = function(e) {
        TouchFix = [
            e.changedTouches[0].pageX,
            e.changedTouches[0].pageY
        ];

    };

    let TouchEnd = function(e) {
        if (!TouchFix) {
            return;
        }
        let dx = e.changedTouches[0].pageX - TouchFix[0];
        let dy = e.changedTouches[0].pageY - TouchFix[1];
        TouchFix = null;
        if (Math.abs(dx) < document.body.clientWidth / 4) {
            return;
        }
        if (dy && Math.abs(dx / dy) < 4) {
            return;
        }
        if (dx < 0) {
            NextPage();
        } else {
            PrevPage();
        }

    };

    window.onload = Init;
    document.onkeydown = KeyDown;

    let Log = function() {
        if (!Params.debug[0]) {
            return;
        }
        console.log.apply(console, arguments);
    };

    let Methods = {
        page: function(pageId) {
            let prevState = CurrentPage ? Pages.get(CurrentPage).state : State;
            CurrentPage = pageId;
            if (!Pages.has(pageId)) {
                Pages.set(pageId, new Page(pageId));
                PagesList.push(pageId);
            }

            let page = Pages.get(pageId);
            prevState.forEach(function(itemProperties, itemId) {
                let properties = {
                    from: {},
                    to: {},
                };
                for (let property in itemProperties.from) {
                    properties.from[property] = itemProperties.from[property];
                }
                for (let property in itemProperties.to) {
                    properties.from[property] = itemProperties.to[property];
                }
                page.state.set(itemId, properties);
            });

            return page;
        },
        item: function(itemId) {
            if (!(itemId in Items)) {
                Items[itemId] = new Proxy(new Item(itemId), {
                    get: function(target, name, receiver) {
                        return function() {
                            return target.call(
                                name,
                                Array.prototype.slice.call(arguments),
                                receiver
                            );
                        }
                    }
                });
            }

            return Items[itemId];
        },
    };

    let State = new Map();

    let FixState = function(itemId, property, value) {
        let storage;
        if (CurrentPage) {
            storage = Pages.get(CurrentPage).state;
        } else {
            storage = State;
        }
        if (!storage.has(itemId)) {
            storage.set(itemId, {
                from: {},
                to: {},
            });
        }
        let itemState = storage.get(itemId);
        itemState.to[property] = value;
    };

    let PrepareItem = function(item, properties, callback) {
        let debug = '';
        let property;
        for (property in properties) {
            let value = properties[property];
            if (property in ItemPropertySet) {
                ItemPropertySet[property].apply(item.self(), value);
            } else if (property in ItemProperties) {
                let style = ItemProperties[property] === null ? property : ItemProperties[property];
                item.div().style[style] = value;
            }

            if (Params.debug[0]) {
                switch (property) {
                    case 'width':
                    case 'height':
                    case 'left':
                    case 'right':
                    case 'top':
                    case 'bottom':
                        debug += ' ' + property.substr(0, 1).toUpperCase() + ':' + (value === null ? 'auto' : parseInt(value));
                }
            }
        }

        if (Params.debug[0] && debug) {
            item.div().setAttribute('data-debug', item.self().name + debug);
        }

        if (callback) {
            callback();
        }
    };

    let AfterPrepare = function() {

    };

    let ShowPage = function(pageId, back) {
        if (!Pages.has(pageId)) {
            return false;
        }

        Log('SHOW PAGE ' + pageId);
        location.hash = pageId;

        if (Pager && Pagination && pageId in Pagination) {
            Pager.style.left = Pagination[pageId][0] + '%';
            Pager.style.width = Pagination[pageId][1] + '%';
        }
        Subtitles.innerText = Pages.get(pageId).subtitles || '';

        CurrentPage = pageId;

        // @todo все анонимные функции вынести в переменную
        Pages.get(pageId).state.forEach(function(itemProperties, itemId) {
            Log('PREPARE ITEM ' + itemId);
            if (!(itemId in Items)) {
                return false;
            }

            let item = Items[itemId];
            if (back) {
                PrepareItem(item, itemProperties.to);
            } else {
                Container.setAttribute('data-prepare', 1);
                PrepareItem(item, itemProperties.from, function () {
                    setTimeout(function () {
                        Container.removeAttribute('data-prepare');
                        PrepareItem(item, itemProperties.to);
                    }, 100)
                });
            }

            return true;
        });

        return true;
    };

    let ParamHooks = {
        resolution: function() { Resize(); },
        backColor: function( color ) {
            Container.style.backgroundColor = color;
        },
        back: function( src ) {
            Container.style.backgroundImage = src ? 'url(' + src + ')' : 'none';
        }
    };

    let JShow = this;

    let Params = {
        resolution: [1024, 768],
        time: null,
        debug: [false],
        backColor: ['#FFF'],
        back: null,
        color: ['#000'],
    };
    let Pages = new Map();
    let Items = {};
    let CurrentPage = null;

    let Resize = function() {
        if (!Container) {
            return;
        }
        Container.setAttribute('data-resize', 1);
        let w = window.innerWidth;
        let h = window.innerHeight;
        let ratio = Params.resolution[0] / Params.resolution[1];
        let fact = w / h;
        let x = 0;
        let y = 0;
        let width = w;
        let height = h;
        if (fact > ratio) {
            width = Math.round(height * ratio);
            x = Math.round((w - width) / 2);
        } else {
            height = Math.round(width / ratio);
            y = Math.round((h - height) / 2);
        }
        Container.style.width = width + 'px';
        Container.style.height = height + 'px';
        Container.style.left = x + 'px';
        Container.style.top = y + 'px';
        Container.style.fontSize = (width / Params.resolution[0] * 40)+ 'px';
        setTimeout(function() { Container.removeAttribute('data-resize'); }, 100);
    };

    let Page = function(pageId) {
        let name = pageId;
        this.state = new Map();
        this.audio = null;
        this.duration = null;
        this.subtitles = null;
    };

    Page.prototype.text = function() {
        this.subtitles = Array.prototype.slice.call(arguments).join("\n");
        return this;
    };

    Page.prototype.sound = function(audioSrc) {
        this.audio = audioSrc;
        return this;
    };

    Page.prototype.time = function(sec) {
        if (parseInt(sec) > 0) {
            this.duration = parseInt(sec);
        }
        return this;
    };

    let Item = function(itemId) {
        this.name = itemId;
        this.div = null;
        this.options = {};
        this.params = {
            relate: null
        };
        this.properties = {};
    };

    let ItemProperties = {
        opacity: null,
        color: null,
        order: 'zIndex'
    };

    let percentSize = function(item, value, axis) {
        if (value === null) {
            return 'auto';
        }
        let parent = Params.resolution;

        if (item.params.relate) {

            let relateItemState = Pages.get(CurrentPage).state.get(item.params.relate);
            parent = [
                (relateItemState.to.width ? relateItemState.to.width[0] : relateItemState.from.width[0] ) || Params.resolution[0],
                (relateItemState.to.height ? relateItemState.to.height[0] : relateItemState.from.height[0]) || Params.resolution[1]
            ];
        }

        return  (value * 100 / parent[axis]) + '%';
    };

    let addTransform = function (div, func, value) {
        let transform = div.style.transform ? div.style.transform : 'perspective(100vh)';
        let result = func + '(' + value + ')';
        if (!transform) {
            div.style.transform = result;
        } else if (transform.search(new RegExp(func + '\\(')) >= 0) {
            div.style.transform = transform.replace(new RegExp(func + '\\([^\\)]*\\)'), result);
        } else {
            div.style.transform = transform + ' ' + result;
        }
    };

    let ItemPropertySet = {
        fontSize: function(value) {
            this.div.style.fontSize = parseFloat(value) + 'em';
        },
        width: function(value) {
            this.div.style.width = percentSize(this, value, 0);
        },
        height: function(value) {
            this.div.style.height = percentSize(this, value, 1);
        },
        // @todo сделать в зависимости от центра
        top: function(value) {
            this.div.style.top = percentSize(this, value, 1);
        },
        bottom: function(value) {
            this.div.style.bottom = percentSize(this, value, 1);
        },
        left: function(value) {
            this.div.style.left = percentSize(this, value, 0);
        },
        right: function (value) {
            this.div.style.right = percentSize(this, value, 0);
        },
        rotate: function (value) {
            addTransform(this.div, 'rotate', parseFloat(value)+ 'deg');
        },
        rotateX: function (value) {
            addTransform(this.div, 'rotateX', parseFloat(value)+ 'deg');
        },
        rotateY: function (value) {
            addTransform(this.div, 'rotateY', parseFloat(value)+ 'deg');
        },
        zoom: function (value) {
            addTransform(this.div, 'scale', parseFloat(value));
        },
        round: function() {
            let round = arguments;
            let radius = '';
            for (let i = 0; i < round.length; i++) {
                if (round[i] < 0) {
                    round[i] = 0;
                } else if (round[i] > 100) {
                    round[i] = 100;
                }
                radius += (round[i]/2) + '% ';
            }

            this.div.style.borderRadius = radius;
        },
        backColor: function() {
            let backgroundColor = null;
            switch (arguments.length) {
                case 1:
                    backgroundColor = arguments[0];
                    break;
                case 3:
                    backgroundColor = 'rgb(' + arguments[0] + ',' + arguments[1] + ',' + arguments[2] + ')';
                    break;
                case 4:
                    backgroundColor = 'rgba(' + arguments[0] + ',' + arguments[1] + ',' + arguments[2] + ',' + arguments[3] + ')';
                    break;
            }
            if (backgroundColor) {
                this.div.style.backgroundColor = backgroundColor;
            }
        },
    };

    Item.prototype.call = function(name, args, receiver) {
        let result;
        if (name in this.methods) {
            result = this.methods[name].apply(this, args);
        } else if (name in ItemProperties || name in ItemPropertySet) {
            this.properties[name] = args.length !== 1 || name in ItemPropertySet ? args : args[0];
            FixState(this.name, name, this.properties[name]);
        } else if (name in this.params) {
            this.params[name] = args[0];
        } else {
            if (args.length) {
                this.options[name] = args;
            } else {
                result = this.options[name];
            }
        }

        return typeof result === 'undefined' ? receiver : result;
    };

    Item.prototype.methods = {
        create: function() {
            if (!this.div) {
                let item = this;
                this.div = document.createElement('div');
                this.div.setAttribute('id', 'jshow__item-' + this.name);
                if (Params.debug[0]) {
                    this.div.setAttribute('data-debug', this.name);
                }
                this.div.setAttribute('class', 'jshow__item');

                this.div.onclick = function(e) {
                    ItemOnClick.call(item, e);
                };
                this.div.ontouchend = function(e) {
                    ItemOnClick.call(item, e);
                };
                for (let option in this.options) {
                    if (this.optionHooks.hasOwnProperty(option)) {
                        this.optionHooks[option].apply(this, this.options[option]);
                    }
                }

                if (this.params.relate) {
                    document.getElementById('jshow__item-' + this.params.relate).appendChild(this.div);
                } else {
                    Container.appendChild(this.div);
                }
            }
        },
        show: function() {
            this.properties.opacity = 1;
            FixState(this.name, 'opacity', 1);
            if (!this.properties.hasOwnProperty('order') || this.properties.order < 0) {
                this.properties.order = 1;
                FixState(this.name, 'order', 1);
            }
        },
        hide: function() {
            this.properties.opacity = 0;
            FixState(this.name, 'opacity', 0);
            this.properties.order = -1;
            FixState(this.name, 'order', -1);
        },
        div: function() {
            return this.div;
        },
        self: function() {
            return this;
        },
    };

    Item.prototype.optionHooks = {
        text: function(text) {
            this.div.innerText = Array.prototype.slice.call(arguments).join("\n");
        },
        html: function(html) {
            this.div.innerHTML = html;
        },
        type: function(type) {
            this.div.setAttribute('data-type', type);
        },
        link: function(link) {
            this.div.setAttribute('data-link', link);
        },
        img: function(src) {
            this.div.innerHTML = '<img src="' + src + '">';
        },
        align: function(align) {
            this.div.setAttribute('data-align', align);
        },
        center: function(x, y) {
            this.div.style.transformOrigin = parseFloat(x) + '% ' + parseFloat(y) + '%';
        },
        back: function(value) {
            this.div.style.backgroundImage = 'url(' + value + ')';
        },
        video: function(src, type) {
            this.div.innerHTML = '<video width="100%" height="100%" controls><source src="' + src + '" type="video/' + (type || 'mp4') + '">Can\'t play video</video>';
        }
    };

    let ItemOnClick = function(e) {
        if (!this.div.hasAttribute('data-link') || this.div.style.zIndex < 0) {
            return true;
        }
        window.open(this.div.getAttribute('data-link'), '_blank');
        return false;
    };

    this.call = function(name, args, receiver) {
        let result;
        if (name in Methods) {
            result = Methods[name].apply(this, args);
        } else if (name in Params) {
            if (args.length) {
                Params[name] = args;
            } else {
                result = Params[name];
            }
        }

        return typeof result === 'undefined' ? receiver : result;
    };
}, {
    get: function(target, name, receiver) {
        return function() {
            return target.call(
                name,
                Array.prototype.slice.call(arguments),
                receiver
            );
        }
    }
});